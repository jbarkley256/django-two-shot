# Generated by Django 4.1.2 on 2022-10-20 15:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0002_rename_receipts_receipt"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="total",
            field=models.DecimalField(decimal_places=3, max_digits=10),
        ),
    ]
